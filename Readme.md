# Sankalp Singh_CVDT

# Directory Structure  
The main folder contains 2 folders  
- PS 1
- PS 2  
  
The PS 1 folder has  
- _Readme.md_ file which tells how to set up the environment for execution
- _Approach_explantion.md_ file which explains the approach which I took
- _yolov4_ folder which has the the config files needed for running the model
- code files which are written in python  
- _requirements.txt_ file which has all the dependencies needed to run the project  
  
The PS 2 folder has  
- _Readme.md_ file which tells how to set up the environment for execution
- _Approach_explantion.md_ file which explains the approach which I took
- code file which is written in python
- _requirements.txt_ file which has all the dependencies needed to run the project  
  
# Problem Statements
1. Track the person which appears in the video
2. Develop a pythonic solution which can rotate the frames of the video, convert them to grayscale and save the output  
