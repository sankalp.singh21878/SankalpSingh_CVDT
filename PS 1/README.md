# Terrorist tracker
 
# Setup
For setting up the the project
- Clone this repository
- Set up a virtual environment (Optional but preferable)
- Download the required libraries using the below mentioned command by using the _requirements.txt_ file provided 

**pip install -r requirements.txt**  
or   
**python -m pip install -r requirements.txt**  

# Running the project
After installation of the libraries open the command prompt and run the main.py file by navagating to the folder where main.py is situated. Example command line instruction is given below
    
**python main.py -i input path of the video to be used -o output path for saving the output video after tracking** 

# Notes
Refer the _Approach_explanation.md_ file for seeing the challenges and their solutions

