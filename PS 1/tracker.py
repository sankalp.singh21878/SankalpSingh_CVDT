"""
@ Author: Sankalp Singh
@ filename: tracker.py
"""

import numpy as np
import cv2
from FPS import FPS

# Class containing all the functions required to running the tracker program
class TerroristTracker:
    """
    Constructor
    Arguments : 1. .weight file path
                2. .names file path
                3. .cfg file path
                4. video file path
                5. output file path
    """

    def __init__(self, yoloweight, classnames, yolocfg, inputfile, outputfile):
        # Initializing some variables
        self.yoloWeight = yoloweight  # model weight file path
        self.classNames = classnames  # class names file path
        self.yoloCfg = yolocfg  # cfg file path
        self.conf = 0.35  # confidence level for detecting people
        self.threshold = 0.5  # minima suppression threshold level
        self.classLabels = open(self.classNames).read().strip().split("\n")  # class label array

        np.random.seed(42)
        self.COLORS = np.random.randint(0, 255, size=(len(self.classLabels), 3), dtype="uint8")  # colour array

        self.model = cv2.dnn.readNetFromDarknet(self.yoloCfg, self.yoloWeight)  # detection model
        self.ln = self.model.getLayerNames()  # layers of the model
        self.ln = [self.ln[i - 1] for i in self.model.getUnconnectedOutLayers()]

        self.writer = None  # writer pointer
        (self.W, self.H) = (None, None)

        self.fps = FPS()  # object for the FPS package

        self.inputPath = inputfile  # video file path
        self.outputPath = outputfile  # output file path

        self.frame = None
        self.video = None
        self.boxes = None

        self.confidences = None
        self.classIDs = None

    '''
    Function to read the video file
    '''

    def readVideoFile(self):
        self.video = cv2.VideoCapture(self.inputPath)
        ret, _ = self.video.read()
        return ret

    '''
    Function to loop through each frames of video
    '''

    def LoopingFrames(self):
        # running the loop till there are frames to be processed in the video
        while True:
            self.fps.start()  # starting the FPS counter
            ret, self.frame = self.video.read()  # reading the frame of the video

            # checking whether we were able to read the frame
            if not ret:
                break

            # Assigning the width and height of the frame
            if self.W is None or self.H is None:
                (self.H, self.W) = self.frame.shape[:2]

            # Running the detect function for the current frame
            self.detect()

            # Putting a bounding box on the detected person
            self.boundingBox()

            # Calculating the FPS and putting it on the frame
            self.fps.stop()
            self.fps.calculateFPS()
            self.frame = self.fps.textFPS(self.frame)

            # Writing the frame to the output file
            if self.writer is None:
                fourcc = cv2.VideoWriter_fourcc("A", "V", "C", "1")
                self.writer = cv2.VideoWriter(self.outputPath, fourcc, 30, (self.frame.shape[1], self.frame.shape[0]),
                                              True)

            self.writer.write(self.frame)

        # Releasing the resources after all of the frames are done
        self.writer.release()
        self.video.release()

    '''
    Function to run detection
    '''

    def detect(self):
        # Constructing a blob from the input image and passing it forward through the YOLO object detector to get the
        # bounding boxes and probabilities
        imageBlob = cv2.dnn.blobFromImage(self.frame, 1 / 255.0, (416, 416), swapRB=True, crop=False)
        self.model.setInput(imageBlob)
        layerOutputs = self.model.forward(self.ln)

        # Initialize our list of bounding boxes, confidences and classIDs
        self.boxes = []
        self.confidences = []
        self.classIDs = []

        for output in layerOutputs:
            # Looping over each detections
            for detection in output:
                # Extracting confidence and class ID of the current detection
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]
                # Filtering out the weak probabilities
                if confidence > self.conf:
                    # Scaling the bounding boxes respective to the image detected since YOLO returns the center of the
                    # detected object followed by the width and height
                    box = detection[0:4] * np.array([self.W, self.H, self.W, self.H])
                    (centerX, centerY, width, height) = box.astype("int")

                    # Using the center coordinates deriving the top and left corner of the bounding box
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    # Appending them to our initialized list which will be used to draw the bounding box on the detected
                    # object
                    self.boxes.append([x, y, int(width), int(height)])
                    self.confidences.append(float(confidence))
                    self.classIDs.append(classID)

    '''
    Function to put a bounding box on the detected person
    '''

    def boundingBox(self):
        # Suppressing multiple boxes and overlapping bounding boxes using minima suppression
        ids = cv2.dnn.NMSBoxes(self.boxes, self.confidences, self.conf, self.threshold)

        # Making sure there is at least one detection
        if len(ids) > 0:
            # Looping over the indexes we are keeping
            for i in ids.flatten():
                # Extract the bounding box coordinates
                (x, y) = (self.boxes[i][0], self.boxes[i][1])
                (w, h) = (self.boxes[i][2], self.boxes[i][3])
                # Checking if the detected object is a person. If it is a person the draw a bounding box over the person
                # and put label and the confidence relative to that
                color = [int(c) for c in self.COLORS[self.classIDs[i]]]
                if self.classIDs[i] == 0:
                    cv2.rectangle(self.frame, (x, y), (x + w, y + h), color, 2)
                    text = "{}: {:.4f}".format(self.classLabels[self.classIDs[i]], self.confidences[i])
                    cv2.putText(self.frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    '''
    Function to execute the algorithm
    '''

    def run(self):
        # Calling the read video function to read the video file first and check if it
        # was successful if yes then call the looping function else quit
        if self.readVideoFile():
            self.LoopingFrames()  # Calling the looping function to loop over the frames
