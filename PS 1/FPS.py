"""
@Author: Sankalp Singh
@Filename: FPS.py
"""
import cv2

# Class containing all the functions required for calculating the FPS
class FPS:
    def __init__(self):
        self.startTime = None
        self.endTime = None
        self.time = None
        self.fps = None

    '''
    Function for recording the start time
    '''
    def start(self):
        self.startTime = cv2.getTickCount()

    '''
    Function for recording the end time
    '''
    def stop(self):
        self.endTime = cv2.getTickCount()

    '''
    Function for calculating FPS based on the start time, end time and the frequency
    '''
    def calculateFPS(self):
        self.time = (self.endTime - self.startTime) / cv2.getTickFrequency()
        self.fps = 1 / self.time

    '''
    Function for putting the FPS in the video frame
    '''
    def textFPS(self, frame):
        cv2.putText(frame, "FPS: " + str(int(self.fps)), (30, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        return frame
