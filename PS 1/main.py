"""
@Author: Sankalp Singh
@Filename: main.py
@Command: python -i path_to_input_file -o path_to_output_file
@Type: 'python main.py --help' to see all arguments
"""

import argparse
from tracker import TerroristTracker

# Construct argument parser and parse them
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True, help="path to input video")
ap.add_argument("-o", "--output", required=True, help="path to output video")
args = vars(ap.parse_args())

# Define the paths for the weight, cfg and name file for the model
yoloWeights = "C:\\Users\\super\\OneDrive\\Desktop\\CV-Software-Engineer\\SankalpSingh_CVDT\\yolov4\\yolov4.weights"
classNames = "C:\\Users\\super\\OneDrive\\Desktop\\CV-Software-Engineer\\SankalpSingh_CVDT\\yolov4\\coco.names"
yoloCfg = "C:\\Users\\super\\OneDrive\\Desktop\\CV-Software-Engineer\\SankalpSingh_CVDT\\yolov4\\yolov4.cfg"

# Run the tracker file
if __name__ == "__main__":
    trackerObject = TerroristTracker(yoloWeights, classNames, yoloCfg, args["input"], args["output"])
    trackerObject.run()
