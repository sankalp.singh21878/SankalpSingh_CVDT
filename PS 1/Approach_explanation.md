# Terrorist tracker
 
# Approach to solve the problem
There are two major problems that we have to tackle. 

1. The first one is the small size of the man in the video 
2. The second one is the missing dataset which occurs when we try to solve the first problem using image tiling.

Normally when we have to detect a person or multiple people we can do it using YOLOv3 but here we can't do that because even with low confidence setting the model is only able to the person for a split second which is of no use to us. Hence for tackling this problem I have used YOLOv4 which is a better and accurate version of YOLOv3 which is able to detect the person and track the person for a few seconds with a confidence level of 0.35. But the downside of this is the heavy weight of the model resulting in low FPS.

A better and much efficient solution would be to use image tiling to scrape out the small man in the frame and then create the required custom YOLO files (weights, cfg and name) and use it to detect the person but the problem here is the lack of dataset. With a proper dataset we can create the config files and train the model to achieve much greater accuracy of the model
