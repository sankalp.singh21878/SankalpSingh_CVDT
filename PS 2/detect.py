"""
@Author: Sankalp Singh
@Filename: detect.py
@Command: python detect.py -f path_to_the_video_folder -r -g -o
          Here -r, -g and -o are optional flags
"""

# Importing required libraries
import cv2
import argparse
import os

# Setting up command line flags and arguments
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--folder", help="path to the video folder", required=True)
ap.add_argument("-r", "--rotate", action='store_true', help="Rotate each frame by 90 degrees")
ap.add_argument("-g", "--grayscale", action='store_true', help="Convert to grayscale")
ap.add_argument("-o", "--output", action='store_true', help="Save output as a new video")
args = ap.parse_args()

# Reading files from the folder
files = os.listdir(args.folder)

for i in range(len(files)):
    cap = cv2.VideoCapture(args.folder + "\\" + files[i])

    # Rotating the frame by 90 degree, not converting to grayscale and saving the output
    if args.rotate is True and args.grayscale is False and args.output is True:
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

        newVideoR = cv2.VideoWriter('output {0}.mp4'.format(i), cv2.VideoWriter_fourcc("A", "V", "C", "1"), 29,
                                    (height, width), True)

        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                new = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
            else:
                break
            cv2.imshow('output', new)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            newVideoR.write(new)
        newVideoR.release()

    # Not rotating the frame by 90 degree, converting it to grayscale and saving the output
    if args.rotate is False and args.grayscale is True and args.output is True:
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

        newVideoR = cv2.VideoWriter('output {0}.mp4'.format(i), cv2.VideoWriter_fourcc("A", "V", "C", "1"), 29,
                                    (width, height), False)
        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                new = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            else:
                break

            cv2.imshow('output', new)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            newVideoR.write(new)
        newVideoR.release()

    # Rotating the frame by 90 degree, converting it to grayscale and saving the output
    if args.rotate is True and args.grayscale is True and args.output is True:
        frame_number = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

        newVideoR = cv2.VideoWriter('output {0}.mp4'.format(i), cv2.VideoWriter_fourcc("A", "V", "C", "1"), 29,
                                    (height, width), False)
        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                new = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
                new = cv2.cvtColor(new, cv2.COLOR_BGR2GRAY)
            else:
                break
            cv2.imshow('output', new)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            newVideoR.write(new)
        newVideoR.release()

    # Rotating the frame by 90 degree, not converting to grayscale and not saving the output
    if args.rotate is True and args.grayscale is False and args.output is False:
        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                new = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
            else:
                break
            cv2.imshow('output', new)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    # Not rotating the frame by 90 degree, converting it to grayscale and not saving the output
    if args.rotate is False and args.grayscale is True and args.output is False:
        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                new = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            else:
                break

            cv2.imshow('output', new)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    # Rotating the frame by 90 degree, converting it to grayscale and not saving the output
    if args.rotate is True and args.grayscale is True and args.output is False:
        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                new = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
                new = cv2.cvtColor(new, cv2.COLOR_BGR2GRAY)
            else:
                break
            cv2.imshow('output', new)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    cap.release()  # releasing the video object
