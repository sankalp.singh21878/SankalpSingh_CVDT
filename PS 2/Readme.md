# Basic CV System
 
# Setup
For setting up the the project
- Clone this repository
- Set up a virtual environment (Optional but preferable)
- Download the required libraries using the below mentioned command by using the _requirements.txt_ file provided 

**pip install -r requirements.txt**  
or   
**python -m pip install -r requirements.txt**  

# Running the project
After installation of the libraries open the command prompt and run the detect.py file by navigating to the folder where detect.py is situated. Example command line instruction is given below
    
**python detect.py -f path_to_the_video_folder -r -g -o**  
Here -r, -g and -o are optional flags  
- **-r flag**: This flag is used to enable rotaion option
- **-g flag**: This flag is used to enable grayscale option
- **-o flag**: this flag is used to enable output option  

# Notes
Refer the _Approach_explanation.md_ file for seeing the challenges and their solutions

